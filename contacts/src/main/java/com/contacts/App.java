package com.contacts;

import com.contacts.business.ContactManager;
import com.contacts.entity.Contact;

import java.util.ArrayList;
/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        ContactManager cm = new ContactManager();

        Contact cont1 = new Contact("Ivan", "Ivanov", "+7-912-234-56-98", "iv@iv.com");
        Contact cont2 = new Contact("Petr", "Petrov", "+7-909-233-11-91", "pett@google.com");
        Contact cont3 = new Contact("Maksim", "Lobanov", "+7-911-111-56-16", "mak@lob.com");
        Contact cont4 = new Contact("Sergey", "Grachev", "+7-982-109-23-39", "serg@god.com");

        System.out.println("==== Contacts are created successfully! ====");
        System.out.println(cont1.toString());
        System.out.println(cont2.toString());
        System.out.println(cont3.toString());
        System.out.println(cont4.toString());
        System.out.println("");

        System.out.println("==== Adding contacts to the store ====");
        Long id1 = cm.addContact(cont1);
        Long id2 = cm.addContact(cont2);
        Long id3 = cm.addContact(cont3);
        Long id4 = cm.addContact(cont4);

        ArrayList<Contact> contacts = cm.getContacts();
        printContacts(contacts);
        System.out.println("");

        System.out.println("==== Updating contact ====");
        Contact newCnt = new Contact(id1, "Ivan", "Ivanov", "+7-912-234-00-00", "iv@iv.com");
        if(cm.updateContact(newCnt)) {
            System.out.println("Updated contact: " + cm.getContact(id1).toString());
            printContacts(contacts);
        }
        System.out.println("");

        System.out.println("==== Deleting contact ====");
        if (cm.delete(id2)) {
            printContacts(contacts);
        }
    }

    private static void printContacts(ArrayList<Contact> contacts){
        for (Contact cnt : contacts) {
            System.out.println(cnt.toString());
        }
    }
}
