package com.contacts.dao;

import com.contacts.entity.Contact;

import java.util.ArrayList;

public class ContactSimpleDAO implements ContactDAO {
    final private ArrayList<Contact> contacts;

    public ContactSimpleDAO() {
        this.contacts = new ArrayList<Contact>();
    }

    public Long addContact(Contact contact) { // TODO: think about exception
        Long id = generateId();
        contact.setId(id);
        if (contacts.add(contact)) {
            return id;
        }
        return 0L; // TODO: handle!
    }

    public boolean deleteContact(Long id) {
        Contact cnt = getContact(id);
        return contacts.remove(cnt);
    }

    public boolean updateContact(Contact contact) {
        Contact cnt = getContact(contact.getId());
        if (cnt != null) {
            cnt.setFirstName(contact.getFirstName());
            cnt.setLastName(contact.getLastName());
            cnt.setPhone(contact.getPhone());
            cnt.setEmail(contact.getEmail());
        }
        return true; // TODO: handle!
    }

    public Contact getContact(Long id) {
        for (Contact cnt : contacts) {
            if (cnt.getId().equals(id)) {
                return cnt;
            }
        }
        return null; // TODO: handle!
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public Long generateId() {
        long contactId = Math.round(Math.random() * 1000 + System.currentTimeMillis());
        while (getContact(contactId) != null) {
            contactId = Math.round(Math.random() * 1000 + System.currentTimeMillis());
        }

        return contactId;
    }
}
