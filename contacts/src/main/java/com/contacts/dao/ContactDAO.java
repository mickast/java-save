package com.contacts.dao;

import com.contacts.entity.Contact;

import java.util.ArrayList;

public interface ContactDAO {
    public Long addContact(Contact contact);
    public boolean deleteContact(Long id);
    public boolean updateContact(Contact contact);
    public Contact getContact(Long id);
    public ArrayList<Contact> getContacts();
}
