package com.contacts.business;

import com.contacts.dao.ContactDAO;
import com.contacts.dao.ContactDAOFactory;
import com.contacts.dao.ContactSimpleDAO;
import com.contacts.entity.Contact;

import java.util.ArrayList;

public class ContactManager {
    final private ContactDAO dao;

    public ContactManager() {
        this.dao = ContactDAOFactory.getContactDAO();
    }

    public Long addContact(Contact contact) {
        return dao.addContact(contact);
    }

    public boolean delete(Long id) {
        return dao.deleteContact(id);
    }

    public boolean updateContact(Contact contact) {
        return dao.updateContact(contact);
    }

    public Contact getContact(Long id) {
        return dao.getContact(id);
    }

    public ArrayList<Contact> getContacts() {
        return dao.getContacts();
    }
}
